def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board_index):
    print_board(board)
    print("GAME OVER!", board[board_index], "has won")
    exit()


def is_row_winner(board, row):
    if board[0] == board[1] and board[1] == board[2] and row == 1:
        return True
        
    elif board[3] == board[4] and board[4] == board[5] and row == 2:
        return True
        
    elif board[6] == board[7] and board[7] == board[8] and row == 3:
        return True

def is_column_winner(board, column):
    if board[0] == board[3] and board[3] == board[6] and column == 1:
        return True

    elif board[1] == board[4] and board[4] == board[7] and column == 2:
        return True

    elif board[2] == board[5] and board[5] == board[8] and column == 3:
        return True

def is_diagonal_winner(board, diagonal):
    if board[0] == board[4] and board[4] == board[8] and diagonal == 1:
        return True
        
    if board[2] == board[4] and board[4] == board[6] and diagonal == 2:
        return True



board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board, 1):
        game_over(0)

    elif is_row_winner(board, 2):
        game_over(3)

    elif is_row_winner(board, 3):
        game_over(6)

    elif is_column_winner(board, 1):
        game_over(0)

    elif is_column_winner(board, 2):
        game_over(1)

    elif is_column_winner(board, 3):
        game_over(2)

    elif is_diagonal_winner(board, 1):
        game_over(0)

    elif is_diagonal_winner(board, 2):
        game_over(2)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
